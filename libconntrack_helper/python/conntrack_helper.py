##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-
from ctypes import cdll, c_uint32, c_uint16, c_uint8, POINTER, Structure, c_int

import os
path = os.path.dirname(os.path.realpath(__file__))

library = cdll.LoadLibrary(path + "/libconntrack_helper.so")

class connection_info_ipv6_t(Structure):
    _fields_ = (
        ("src", c_uint32 *4),
        ("dst", c_uint32 * 4),
        ("sport", c_uint16),
        ("dport", c_uint16),
        ("l4proto", c_uint8)
    )
connection_info_ipv6_p = POINTER(connection_info_ipv6_t)

getConnectionInfo = library.getConnectionInfo
getConnectionInfo.restype = connection_info_ipv6_t

isAvailable = library.isAvailable
isAvailable.restype = c_int

start = library.start
start.restype = c_int

stop = library.stop

def to128(addr):
    return (addr[0] << 96 | addr[1] << 64 | addr[2] << 32 | addr[3])
