##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-
'''
@TODO: Do we need this class?
'''

import random
import log

class AddressGenerator:
	"""
	This is the base class for all address generating implementations.
	"""
	def __init__(self, prefixes=None, addresses=None):
		"""
		@param prefixes: prefixes to add to the pool
		@param addresses: addresses to add to the pool
		"""
		self.prefixes = prefixes
		self.addresses = addresses

	def pop(self):
		"""
		Get an address and remove it from the pool
		"""
		pass

	def getAddress(self):
		"""
		Get an address
		"""
		pass

class AddressGenerator_random(AddressGenerator):
	"""
	This class generates a random address in the given range.
	"""
	def __init__(self, prefixes=None, addresses=None):
		AddressGenerator.__init__(self, prefixes, addresses)

		if prefixes is None:
			log.error("The random addressGenerator needs at least one prefix. This will not work.")

	def getAddress(self):
		prefixIndex = int(random.random() * (len(self.prefixes)))
		network = self.prefixes[prefixIndex]
		addressIndex = random.randrange(0, network.num_addresses -1)
		address = network[addressIndex]
		return address

	def pop(self):
		return self.getAddress()
