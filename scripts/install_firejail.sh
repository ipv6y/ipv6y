#!/bin/bash
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

git clone https://github.com/netblue30/firejail.git firejail.git
pushd firejail.git
git checkout 0.9.48
export CFLAGS="${CFLAGS/-fsanitize=undefined/}"
./configure --prefix=/usr
make -j5
make install
popd
rm -rf firejail.git
