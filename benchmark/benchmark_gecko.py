#!/usr/bin/env python3

"""
Use Selenium to Measure Web Timing
Performance Timing Events flow
navigationStart -> redirectStart -> redirectEnd -> fetchStart -> domainLookupStart -> domainLookupEnd
-> connectStart -> connectEnd -> requestStart -> responseStart -> responseEnd
-> domLoading -> domInteractive -> domContentLoaded -> domComplete -> loadEventStart -> loadEventEnd
"""

import sys
import os
from argparse import ArgumentParser, RawTextHelpFormatter

import threading
import queue #thread safe

from selenium.webdriver import Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as expected
from selenium.webdriver.support.wait import WebDriverWait

geckodriver = '/tmp/geckodriver'

def test_homepage(url):

	options = Options()
	options.add_argument('-headless')
	driver = Firefox(executable_path=geckodriver, firefox_options=options)
	wait = WebDriverWait(driver, timeout=10)

	driver.get(url)

	navigationStart = driver.execute_script("return window.performance.timing.navigationStart")
	fetchStart = driver.execute_script("return window.performance.timing.fetchStart")
	domainLookupStart = driver.execute_script("return window.performance.timing.domainLookupStart")
	domainLookupEnd  = driver.execute_script("return window.performance.timing.domainLookupEnd")
	connectStart  = driver.execute_script("return window.performance.timing.connectStart")
	connectEnd = driver.execute_script("return window.performance.timing.connectEnd")
	requestStart = driver.execute_script("return window.performance.timing.requestStart")
	responseStart = driver.execute_script("return window.performance.timing.responseStart")
	responseEnd = driver.execute_script("return window.performance.timing.responseEnd")
	domLoading = driver.execute_script("return window.performance.timing.domLoading")
	domInteractive = driver.execute_script("return window.performance.timing.domInteractive")
	domContentLoadedEventStart = driver.execute_script("return window.performance.timing.domContentLoadedEventStart")
	domContentLoadedEventEnd = driver.execute_script("return window.performance.timing.domContentLoadedEventEnd")
	domComplete = driver.execute_script("return window.performance.timing.domComplete")
	loadEventStart = driver.execute_script("return window.performance.timing.loadEventStart")
	loadEventEnd = driver.execute_script("return window.performance.timing.loadEventEnd")

	driver.quit()

	return (navigationStart, fetchStart, domainLookupStart, domainLookupEnd, connectStart, connectEnd, requestStart, responseStart, responseEnd, domLoading, domInteractive, domContentLoadedEventStart, domContentLoadedEventEnd, domComplete, loadEventStart, loadEventEnd)


def benchmark(url, thread_num, samples):
	#setup
	#contains a queue with all thread results
	samples_list = []
	

	#take samples
	for i in range(0, samples):
		data = queue.Queue()
		func = lambda q, arg : q.put(test_homepage(arg))
		print("Taking sample %d..." % i)
		threads = []
		#start all threads
		for j in range(0, thread_num):
			threads.append(threading.Thread(target=func, args=(data, url)))
			threads[j].start()
		#wait for all threads to finish
		for j in range(0, thread_num):
			threads[j].join()
		
		#convert queue to list for simple handling
		samples_list.append(list(data.queue))

	for i in range(0, samples):
		backend_sum = 0
		frontend_sum = 0
		for j in range(0, thread_num):
			backendPerformance = samples_list[i][j][7] - samples_list[i][j][0]
			frontendPerformance = samples_list[i][j][13] - samples_list[i][j][7]
			#print("backend %d" % backendPerformance)
			backend_sum += backendPerformance
			frontend_sum += frontendPerformance
		
		print("Sample %d results:" % i)
		print("avg backend: %d" % (backend_sum/thread_num))
		print("avg frontend: %d" % (frontend_sum/thread_num))

def main(argv = None):
	if argv is None:
		argv = sys.argv
	else:
		sys.argv.extend(argv)
		
	parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
	parser.add_argument("-u", "--url", dest="url", default = None, help="Set the url to test", type=str)
	parser.add_argument("-s", "--samples", dest="samples", default = 1, help="Number of samples to take [default: %(default)s]", type=int) 
	parser.add_argument("-t", "--threads", dest="threads", default = 1, help="Number of threads per sample [default: %(default)s]", type=int)
	
	args = parser.parse_args()
	url = args.url
	samples = args.samples
	threads = args.threads

	if url is None:
		parser.print_help()
		os._exit(1)
	
	print("Benchmarking %s with %d threads and %d samples" %(url, threads, samples))
	benchmark(url, threads, samples)
	
	#TODO: export to json and plot
	#TODO: merge with existing benchmark script


if __name__ == '__main__': 	
	main()
