##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

import time
import SysRandom
import socket
import tldextract
from pyblake2 import blake2s

class Bouquet:
	"""
	This class is the base class for all bouquet implementations
	@todo: Make methods abstract
	"""
	def __init__(self, addressGenerator):
		self.addressGenerator = addressGenerator

	def getAddress(self):
		"""
		Get a new address to use for masquerading
		@return: a new address
		"""
		return ""

class Bouquet_connection(Bouquet):
	"""
	This bouquet changes the address for every new connection.
	"""
	def __init__(self, addressGenerator):
		Bouquet.__init__(self, addressGenerator)

	def getAddress(self, connection):
		"""
		Get an address for the given connection
		"""
		address = self.addressGenerator.pop()
		return address


class Bouquet_time(Bouquet):
	"""
	This bouquet changes the address after a given interval.
	@todo: Remove duplicate code
	"""
	def __init__(self, addressGenerator, mininterval, maxinterval = -1):
		"""
		@param maxinterval: can be omitted
		"""
		Bouquet.__init__(self, addressGenerator)
		self.mininterval = mininterval
		self.maxinterval = maxinterval + 1

		if self.maxinterval == 0:
			self.maxinterval = self.mininterval + 1

		self.lastTime = time.time()
		self.currAddr = self.addressGenerator.pop()

	def getAddress(self, connection):
		if (time.time() - self.lastTime) > SysRandom.randrange(self.mininterval, self.maxinterval):
			self.currAddr = self.addressGenerator.pop()
			self.lastTime = time.time()
		return self.currAddr

class Bouquet_destination(Bouquet):
	"""
	This bouquet always returns the same address for the given connection and seed.
	"""
	def __init__(self, addressGenerator):
		Bouquet.__init__(self, addressGenerator)
		self.prefixes = addressGenerator.prefixes
		self.seed = SysRandom.getrandbits(128)

	def getAddress(self, connection, seed=None, dst=None):
		if dst is None:
			dst = connection.destination

		if seed is None:
			seed = self.seed

		#TODO: change calculation?
		val = int(int(dst) ^ seed)
		dst_hash = blake2s(str(val).encode()).hexdigest()
		index = int(dst_hash, 16)

		prefixIndex = index % len(self.prefixes)
		network = self.prefixes[prefixIndex]
		addressIndex = index % network.num_addresses
		address = network[addressIndex] # this is the same as network + addressIndex

		#hex_index = int((128 - network.prefixlen)/4)
		#address = network.network_address + int(dst_hash.hexdigest()[0:hex_index],16)

		return address

class Bouquet_destination_time(Bouquet_destination):
	"""
	This bouquet acts like @ref Bouquet_connection, but changes the seed at a given time interval.
	"""
	def __init__(self, addressGenerator, minInterval, maxInterval=-1):
		Bouquet_destination.__init__(self, addressGenerator)
		self.mininterval = minInterval
		self.maxinterval = maxInterval + 1

		if self.maxinterval == 0:
			self.maxinterval = self.mininterval + 1

		self.seed = SysRandom.getrandbits(128)
		self.lastTime = time.time()

	def getAddress(self, connection):
		if (time.time() - self.lastTime) > SysRandom.randrange(self.mininterval, self.maxinterval):
			self.seed = SysRandom.getrandbits(128)
			self.lastTime = time.time()
		return Bouquet_destination.getAddress(self, connection, seed=self.seed)

class Bouquet_destination_sld(Bouquet_destination):
	"""
	This bouquet acts like @ref Bouquet_connection, but rather than IP-addresses this uses the SLD for identification.
	@attention: This could/will expose too much information
	"""
	def __init__(self, addressGenerator):
		Bouquet_destination.__init__(self, addressGenerator)

	def getAddress(self, connection, seed=0):
		dst = connection.destination
		try:
			dst_host = socket.gethostbyaddr(str(dst))[0]
			tld = tldextract.extract(dst_host)
			dst_sld = tld.registered_domain
		except socket.herror:
			dst_sld = dst

		dst_sld_hash = blake2s(str(dst_sld).encode()).hexdigest()

		return Bouquet_destination.getAddress(self, connection, seed, int(dst_sld_hash, 16))
