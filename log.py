##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

import sys

# Color terminal output if termcolor package is available. It can be installed
# by running: pip install termcolor
try:
	from termcolor import colored
except ImportError:
	def colored(string, color):
		return string

def info(msg):
	print(colored(msg, 'magenta'))

def warning(msg):
	print(colored(msg, 'yellow'))

def error(msg):
	print(colored(msg, 'red'), file=sys.stderr)
