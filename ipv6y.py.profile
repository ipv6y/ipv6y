# Firejail profile for ipv6y
# Persistent local customizations
include /etc/firejail/ipv6y.local
# Persistent global definitions
include /etc/firejail/globals.local

noblacklist ${HOME}/ipv6y
noblacklist /sbin

include /etc/firejail/disable-common.inc
include /etc/firejail/disable-devel.inc
include /etc/firejail/disable-passwdmgr.inc
include /etc/firejail/disable-programs.inc

caps.keep net_admin,net_raw

netfilter
protocol unix,inet,inet6,netlink
seccomp
nonewprivs
nogroups
shell none
nosound
ipc-namespace

private-dev
private-tmp

# noexec ${HOME}
noexec /tmp
