#!/bin/sh -x
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

EXTRA_OPTIONS=$(cat << EOF
OUTPUT_DIRECTORY=./
INPUT=$SCRIPT_DIR/../
EOF
)

command -v py_filter >/dev/null 2>&1 || {

mkdir $SCRIPT_DIR/bin
cat << EOF > $SCRIPT_DIR/bin/py_filter
#!/bin/sh
doxypypy -a -c \$1
EOF
chmod +x $SCRIPT_DIR/bin/py_filter

export PATH=$PATH:$SCRIPT_DIR/bin

}

DOXYGEN_OUTPUT=$( (cat "$SCRIPT_DIR/../doc/Doxyfile" ; echo "$EXTRA_OPTIONS") | doxygen - 2>&1)

echo "$DOXYGEN_OUTPUT"

DOXYGEN_ERROR=$(echo "$DOXYGEN_OUTPUT" | grep "not documented")

if [[ -z "$DOXYGEN_ERROR" ]]
then
	exit 0
else
	exit 0
fi
