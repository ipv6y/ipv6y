##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

import re
from ipaddress import IPv6Interface
from iputils import getRoutes

def getPrefixDelegations(onlyGlobal=False, blacklist = []):
	"""
	Gets all prefixes delegated to the current machine.
	@param onlyGlobal: only use global routable prefixes.
	@param blacklist: don't add these prefixes
	@todo the current implementation is garbage
	"""
	result = []
	route_str = getRoutes().decode("UTF8").split("\n")
	regex = re.compile("^unreachable.*$") #TODO: is there ANY better way to detect our prefixes?

	for line in route_str:
		re_result = re.search(regex, line)
		if re_result is not None:
			prefix = IPv6Interface(re_result.group().split(" ")[1]).network
			if not prefix.is_private or not onlyGlobal:
				add = True
				for entry in blacklist:
					if IPv6Interface(entry).network == prefix:
						add = False
						break
				if add:
					result.append(prefix)
	return result

def interfacename_to_id(interface: str):
	path = "/sys/class/net/{}/ifindex".format(interface)
	with open(path) as f:
		return int(f.readline());

