/*
 * This file is part of ipv6y 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <netinet/in.h>
#include <stddef.h>	/* for offsetof */
#include <inttypes.h>

#include <linux/ipv6.h>
#include <linux/tcp.h>
#include <linux/netfilter.h>
#include <linux/netfilter/nf_tables.h>
#include <linux/netfilter/nfnetlink.h>

#include <libmnl/libmnl.h>
#include <libnftnl/table.h>
#include <libnftnl/chain.h>
#include <libnftnl/rule.h>
#include <libnftnl/expr.h>

#define NFT_CT_NEW 0x8
#define NFT_CT_ESTABLISHED 0x2
#define NFT_CT_RELATED 0x4
#define NFT_CT_INVALID 0x1

#define CALLBACK_GET_HANDLE 0x1

struct callback_data{
	uint32_t type;
	uint64_t results[64]; //TODO: variable size?
	uint32_t len;
};

struct nftnl_handle{
	//struct mnl_socket* nl;
	char *buf;
	uint32_t buf_size;
	uint32_t seq, family, batching;
	struct mnl_nlmsg_batch* batch;
};


static uint32_t event2flag(uint32_t event)
{
	switch (event) {
	case NFT_MSG_NEWTABLE:
	case NFT_MSG_NEWCHAIN:
	case NFT_MSG_NEWRULE:
	case NFT_MSG_NEWSET:
	case NFT_MSG_NEWSETELEM:
	case NFT_MSG_NEWGEN:
		return NFTNL_OF_EVENT_NEW;
	case NFT_MSG_DELTABLE:
	case NFT_MSG_DELCHAIN:
	case NFT_MSG_DELRULE:
	case NFT_MSG_DELSET:
	case NFT_MSG_DELSETELEM:
		return NFTNL_OF_EVENT_DEL;
	}

	return 0;
}


void add_chain_to_batch(struct nftnl_handle* handle, struct nftnl_chain* chain){
	struct nlmsghdr* nlh = nftnl_chain_nlmsg_build_hdr(mnl_nlmsg_batch_current(handle->batch), NFT_MSG_NEWCHAIN, handle->family,
					NLM_F_CREATE|NLM_F_ACK, handle->seq++);
	nftnl_chain_nlmsg_build_payload(nlh, chain);
	nftnl_chain_free(chain);
	mnl_nlmsg_batch_next(handle->batch);
}

void add_rule_to_batch(struct nftnl_handle* handle, struct nftnl_rule* rule, uint16_t cmd){
	struct nlmsghdr* nlh = nftnl_rule_nlmsg_build_hdr(mnl_nlmsg_batch_current(handle->batch),
					cmd,
					nftnl_rule_get_u32(rule, NFTNL_RULE_FAMILY),
					NLM_F_APPEND|NLM_F_CREATE|NLM_F_ACK, handle->seq++);
	nftnl_rule_nlmsg_build_payload(nlh, rule);
	nftnl_rule_free(rule);
	mnl_nlmsg_batch_next(handle->batch);
}


static void add_payload(struct nftnl_rule *r, uint32_t base, uint32_t dreg,
			uint32_t offset, uint32_t len){
	struct nftnl_expr *e;

	e = nftnl_expr_alloc("payload");
	if (e == NULL) {
		perror("expr payload oom");
		exit(EXIT_FAILURE);
	}

	nftnl_expr_set_u32(e, NFTNL_EXPR_PAYLOAD_BASE, base);
	nftnl_expr_set_u32(e, NFTNL_EXPR_PAYLOAD_DREG, dreg);
	nftnl_expr_set_u32(e, NFTNL_EXPR_PAYLOAD_OFFSET, offset);
	nftnl_expr_set_u32(e, NFTNL_EXPR_PAYLOAD_LEN, len);

	nftnl_rule_add_expr(r, e);
}

static void add_cmp(struct nftnl_rule *r, uint32_t sreg, uint32_t op,
		    const void *data, uint32_t data_len){
	struct nftnl_expr *e;

	e = nftnl_expr_alloc("cmp");
	if (e == NULL) {
		perror("expr cmp oom");
		exit(EXIT_FAILURE);
	}

	nftnl_expr_set_u32(e, NFTNL_EXPR_CMP_SREG, sreg);
	nftnl_expr_set_u32(e, NFTNL_EXPR_CMP_OP, op);
	nftnl_expr_set(e, NFTNL_EXPR_CMP_DATA, data, data_len);

	nftnl_rule_add_expr(r, e);
}

static void add_bitwise(struct nftnl_rule *rule, int reg, const void *mask, size_t len){
	struct nftnl_expr *expr;
	uint8_t *xor;
	expr = nftnl_expr_alloc("bitwise");
	if (expr == NULL) {
			perror("expr bitwise oom");
			exit(EXIT_FAILURE);
		}
	xor = alloca(len);
	memset(xor, 0, len);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_BITWISE_SREG, reg);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_BITWISE_DREG, reg);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_BITWISE_LEN, len);
	nftnl_expr_set(expr, NFTNL_EXPR_BITWISE_MASK, mask, len);
	nftnl_expr_set(expr, NFTNL_EXPR_BITWISE_XOR, xor, len);
	nftnl_rule_add_expr(rule, expr);
}

/*
root@raspberrypi-server /tmp # nft --debug netlink add rule ip6 ipv6y postrouting oif eth0 ip6 daddr ::3 tcp dport 22 snat ::2                         :(
ip6 ipv6y postrouting
  [ meta load oif => reg 1 ]
  [ cmp eq reg 1 0x00000002 ]
  [ payload load 16b @ network header + 24 => reg 1 ]
  [ cmp eq reg 1 0x00000000 0x00000000 0x00000000 0x03000000 ]
  [ payload load 1b @ network header + 6 => reg 1 ]
  [ cmp eq reg 1 0x00000006 ]
  [ payload load 2b @ transport header + 2 => reg 1 ]
  [ cmp eq reg 1 0x00001600 ]
  [ immediate reg 1 0x00000000 0x00000000 0x00000000 0x02000000 ]
  [ nat snat ip6 addr_min reg 1 addr_max reg 0 ]
 */
static struct nftnl_rule* allocate_nat_rule(uint8_t proto, uint32_t addr[4], uint32_t nat[4], uint16_t sport, uint16_t dport, const char* table_str, const char* chain_str, uint32_t interface_index, int issnat){

	struct nftnl_rule* rule;
	struct nftnl_expr* expr;

	uint32_t offset = 0;

	enum nft_nat_types nat_type = NFT_NAT_DNAT;
	if(issnat) nat_type = NFT_NAT_SNAT;

	rule = nftnl_rule_alloc();
	if (rule == NULL) {
		perror("OOM");
		return NULL;
	}

	nftnl_rule_set(rule, NFTNL_RULE_TABLE, table_str);
	nftnl_rule_set(rule, NFTNL_RULE_CHAIN, chain_str);
	nftnl_rule_set_u32(rule, NFTNL_RULE_FAMILY, NFPROTO_IPV6);

	//[ meta load oif => reg 1 ]
	expr = nftnl_expr_alloc("meta");
	if(expr == NULL) {
		perror("OOM");
		return NULL;
	}

	if(issnat){
		nftnl_expr_set_u32(expr, NFTNL_EXPR_META_KEY, NFT_META_OIF);
	}
	else {
		nftnl_expr_set_u32(expr, NFTNL_EXPR_META_KEY, NFT_META_IIF);
	}

	nftnl_expr_set_u32(expr, NFTNL_EXPR_META_DREG, NFT_REG_1);
	nftnl_rule_add_expr(rule, expr);
	//[ cmp eq reg 1 0x00000002 ]
	add_cmp(rule, NFT_REG_1, NFT_CMP_EQ, &interface_index, sizeof(interface_index));
	//[ payload load 16b @ network header + 24 => reg 1 ]
	if(issnat){
		offset = offsetof(struct ipv6hdr, daddr);
	}
	else {
		offset = offsetof(struct ipv6hdr, saddr);
	}
	add_payload(rule, NFT_PAYLOAD_NETWORK_HEADER, NFT_REG_1,
					offset, sizeof(struct in6_addr));

	// [ cmp eq reg 1 0x00000000 0x00000000 0x00000000 0x03000000 ]
	add_cmp(rule, NFT_REG_1, NFT_CMP_EQ, addr, sizeof(uint32_t) * 4);

	// [ payload load 1b @ network header + 6 => reg 1 ]
	add_payload(rule, NFT_PAYLOAD_NETWORK_HEADER, NFT_REG_1,
					offsetof(struct ipv6hdr, nexthdr), sizeof(uint8_t));

	// [ cmp eq reg 1 0x00000006 ]
	add_cmp(rule, NFT_REG_1, NFT_CMP_EQ, &proto, 1);
	// [ payload load 2b @ transport header + 2 => reg 1 ]
	offset = offsetof(struct tcphdr, dest);
	uint32_t offset_tmp = offset;
	offset_tmp = offsetof(struct tcphdr, source);

	if(offset_tmp < offset) offset = offset_tmp;

	add_payload(rule, NFT_PAYLOAD_TRANSPORT_HEADER, NFT_REG_1,
			offset, sizeof(uint32_t));

	// [ cmp eq reg 1 0x00001600 ]
	uint16_t hsport = htons(sport);
	uint16_t hdport = htons(dport);
	uint32_t port = (hdport << 16) | hsport;
	add_cmp(rule, NFT_REG_1, NFT_CMP_EQ, &port, sizeof(uint32_t));

	// [ immediate reg 1 0x00000000 0x00000000 0x00000000 0x02000000 ]
	expr = nftnl_expr_alloc("immediate");
	nftnl_expr_set_u32(expr, NFTNL_EXPR_IMM_DREG, NFT_REG_1);
	nftnl_expr_set(expr, NFTNL_EXPR_IMM_DATA, nat, sizeof(uint32_t) * 4);
	nftnl_rule_add_expr(rule, expr);

	// [ nat snat ip6 addr_min reg 1 addr_max reg 0 ]
	expr = nftnl_expr_alloc("nat");
	nftnl_expr_set_u32(expr, NFTNL_EXPR_NAT_TYPE, nat_type);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_NAT_FAMILY, NFPROTO_IPV6);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_NAT_REG_ADDR_MIN, NFT_REG_1);
	nftnl_rule_add_expr(rule, expr);

	return rule;


}

static struct nftnl_chain* allocate_chain(const char* table_str, const char* chain_str, const char* type_str, uint32_t hooknum, uint32_t priority){
	struct nftnl_chain* chain;

	chain = nftnl_chain_alloc();
	if (chain == NULL) {
		perror("OOM");
		return NULL;
	}
	nftnl_chain_set(chain, NFTNL_CHAIN_TABLE, table_str);
	nftnl_chain_set(chain, NFTNL_CHAIN_NAME, chain_str);
	nftnl_chain_set_u32(chain, NFTNL_CHAIN_HOOKNUM, hooknum);
	nftnl_chain_set_u32(chain, NFTNL_CHAIN_PRIO, priority);
	nftnl_chain_set(chain, NFTNL_CHAIN_TYPE, type_str);

	return chain;
}

static struct nftnl_table* allocate_table(uint32_t family, const char* name){
	struct nftnl_table *t;
	t = nftnl_table_alloc();
	if (t == NULL) {
		perror("OOM");
		return NULL;
	}

	nftnl_table_set_u32(t, NFTNL_TABLE_FAMILY, family);
	nftnl_table_set_str(t, NFTNL_TABLE_NAME, name);

	return t;
}

void open_and_bind_socket(struct mnl_socket** socket){
	struct mnl_socket* nl = mnl_socket_open(NETLINK_NETFILTER);
	if (nl == NULL) {
		perror("mnl_socket_open");
		exit(EXIT_FAILURE);
	}

	if (mnl_socket_bind(nl, (1 << (NFNLGRP_NFTABLES-1)), MNL_SOCKET_AUTOPID) < 0) {
		perror("mnl_socket_bind");
		exit(EXIT_FAILURE);
	}

	*socket = nl;
}

struct nftnl_handle* allocate_handle(){
	struct nftnl_handle* handle = malloc(sizeof(struct nftnl_handle));
	handle->buf = malloc(MNL_SOCKET_BUFFER_SIZE*2); //prevent funny memory corruptions
	handle->buf_size = MNL_SOCKET_BUFFER_SIZE;
	handle->seq = time(NULL);
	handle->batch = mnl_nlmsg_batch_start(handle->buf, handle->buf_size);

	handle->batching = nftnl_batch_is_supported();
	if (handle->batching < 0) {
		perror("cannot talk to nfnetlink");
		return NULL;
	}

	//handle->nl = open_and_bind_socket();

	return handle;
}

void free_handle(struct nftnl_handle* handle){
	if(handle != NULL){

		mnl_nlmsg_batch_stop(handle->batch);
		//mnl_socket_close(handle->nl);

		if(handle->buf != NULL) free(handle->buf);
		free(handle);
	}
}

int rule_cb(const struct nlmsghdr *nlh, uint32_t event, struct callback_data* cb_data){
	struct nftnl_rule *t;

	t = nftnl_rule_alloc();
	if (t == NULL) {
			perror("OOM");
			goto err;
	}

	if (nftnl_rule_nlmsg_parse(nlh, t) < 0) {
			perror("nftnl_rule_nlmsg_parse");
			goto err_free;
	}

	uint64_t handle = nftnl_rule_get_u64(t, NFTNL_RULE_HANDLE);

	if(cb_data){
		if(cb_data->type == CALLBACK_GET_HANDLE){
			cb_data->results[cb_data->len++] = handle; //FIXME: no check for maxlen!!!
		}
	}

err_free:
	nftnl_rule_free(t);
err:
	return MNL_CB_OK;
}

int event_cb(const struct nlmsghdr *nlh, void *data){
	int err = MNL_CB_OK;
	int event = NFNL_MSG_TYPE(nlh->nlmsg_type);
	struct callback_data* cb_data = data;

	 switch(event) {
		case NFT_MSG_NEWCHAIN:
			break;
		case NFT_MSG_NEWRULE:
			err = rule_cb(nlh, event, cb_data);
			break;
		default:
			break;
	 }

	return err;
}

int send_data(struct nftnl_handle* handle, struct callback_data* cb_data){

	struct mnl_socket* nl;

	open_and_bind_socket(&nl);

	if (mnl_socket_sendto(nl, mnl_nlmsg_batch_head(handle->batch),
				  mnl_nlmsg_batch_size(handle->batch)) < 0) {
		perror("mnl_socket_send");
		return -1;
	}

	int ret = mnl_socket_recvfrom(nl, handle->buf, handle->buf_size);
	uint32_t portid = mnl_socket_get_portid(nl);
	while (ret > 0) {
		ret = mnl_cb_run(handle->buf, ret, 0, portid, event_cb, cb_data);
		if (ret <= 0)
			break;
		ret = mnl_socket_recvfrom(nl, handle->buf, handle->buf_size);

	}
	if (ret == -1) {
		perror("error");
		return -1;
	}

	//FIXME: Is it possible to keep the socket open?
	//free_handle(handle);
	//handle = allocate_handle();

	mnl_nlmsg_batch_reset(handle->batch);
	mnl_socket_close(nl);

	return 0;
}

void stop(){

}

void start_batch(struct nftnl_handle* handle){
	if (handle->batching) {
		nftnl_batch_begin(mnl_nlmsg_batch_current(handle->batch), handle->seq++);
		mnl_nlmsg_batch_next(handle->batch);
	}
}

void end_batch(struct nftnl_handle* handle){
	if (handle->batching) {
		nftnl_batch_end(mnl_nlmsg_batch_current(handle->batch), handle->seq++);
		mnl_nlmsg_batch_next(handle->batch);
	}
}

void rule_del(struct nftnl_handle* handle, const char* table, const char* chain, uint64_t rule_handle){

	struct nftnl_rule* rule;

	rule = nftnl_rule_alloc();
	if (rule == NULL) {
		perror("OOM");
	}

	start_batch(handle);
	nftnl_rule_set(rule, NFTNL_RULE_TABLE, table);
	nftnl_rule_set(rule, NFTNL_RULE_CHAIN, chain);
	nftnl_rule_set_u32(rule, NFTNL_RULE_FAMILY, NFPROTO_IPV6);
	nftnl_rule_set_u64(rule, NFTNL_RULE_HANDLE, rule_handle);
	add_rule_to_batch(handle, rule, NFT_MSG_DELRULE);
	end_batch(handle);
	send_data(handle, NULL);
}

void add_nat_pair(struct nftnl_handle* handle, const char* table, uint32_t proto, uint32_t src[4], uint32_t dst[4], uint32_t addr[4], uint16_t sport, uint16_t dport, uint32_t interface, uint64_t* snat_handle, uint64_t* dnat_handle){
	struct nftnl_rule *snat_rule, *dnat_rule;

	snat_rule = allocate_nat_rule(proto, dst, addr, sport, dport, table, "postrouting", interface, 1);
	dnat_rule = allocate_nat_rule(proto, src, addr, dport, sport, table, "prerouting", interface, 0);

	start_batch(handle);

	add_rule_to_batch(handle, snat_rule, NFT_MSG_NEWRULE);
	add_rule_to_batch(handle, dnat_rule, NFT_MSG_NEWRULE);

	end_batch(handle);

	struct callback_data cb_data;
	cb_data.type = CALLBACK_GET_HANDLE;
	cb_data.len = 0;
	send_data(handle, &cb_data);

	if(cb_data.len == 2){
		if(snat_handle) *snat_handle = cb_data.results[0];
		if(dnat_handle) *dnat_handle = cb_data.results[1];
	}
}
/*
# nft --debug netlink add rule ip6 ipv6y forward_nfqueue oif eth0 ct state new queue num 1
ip6 ipv6y forward_nfqueue
  [ meta load oif => reg 1 ]
  [ cmp eq reg 1 0x00000002 ]
  [ ct load state => reg 1 ]
  [ bitwise reg 1 = (reg=1 & 0x00000008 ) ^ 0x00000000 ]
  [ cmp neq reg 1 0x00000000 ]
  [ queue num 1 ]


 */
struct nftnl_rule* allocate_nfqueue_rules(const char* table_str, const char* chain_str, uint32_t output_interface, uint32_t queue_num, uint32_t state){
	struct nftnl_rule* rule;
	struct nftnl_expr* expr;

	rule = nftnl_rule_alloc();
	if (rule == NULL) {
		perror("OOM");
		return NULL;
	}

	nftnl_rule_set(rule, NFTNL_RULE_TABLE, table_str);
	nftnl_rule_set(rule, NFTNL_RULE_CHAIN, chain_str);
	nftnl_rule_set_u32(rule, NFTNL_RULE_FAMILY, NFPROTO_IPV6);


	expr = nftnl_expr_alloc("meta");
	if(expr == NULL) {
		perror("OOM");
		return NULL;
	}
	//[ meta load oif => reg 1 ]
	nftnl_expr_set_u32(expr, NFTNL_EXPR_META_KEY, NFT_META_OIF);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_META_DREG, NFT_REG_1);
	nftnl_rule_add_expr(rule, expr);

	// [ cmp eq reg 1 0x00000002 ]
	add_cmp(rule, NFT_REG_1, NFT_CMP_EQ, &output_interface, sizeof(uint32_t));
	// [ ct load state => reg 1 ]
	expr = nftnl_expr_alloc("ct");
	nftnl_expr_set_u32(expr, NFTNL_EXPR_CT_KEY, NFT_CT_STATE);
	nftnl_expr_set_u32(expr, NFTNL_EXPR_CT_DREG, NFT_REG_1);
	nftnl_rule_add_expr(rule, expr);
	// [ bitwise reg 1 = (reg=1 & 0x00000008 ) ^ 0x00000000 ]
	add_bitwise(rule, NFT_REG_1, &state, sizeof(uint32_t));
	// [ cmp neq reg 1 0x00000000 ]
	uint32_t dummy0 = 0; //TODO: what value is this? just a test for not null?
	add_cmp(rule, NFT_REG_1, NFT_CMP_NEQ, &dummy0, sizeof(uint32_t));
	// [ queue num 1 ]
	expr = nftnl_expr_alloc("queue");
	nftnl_expr_set_u32(expr, NFTNL_EXPR_QUEUE_NUM, queue_num);
	nftnl_rule_add_expr(rule, expr);

	return rule;
}

void create_default_table_chains(struct nftnl_handle* handle, uint32_t interface){
	struct nftnl_table *table;
	struct nftnl_chain *chain_pre, *chain_post, *chain_forward;
	struct nftnl_rule* nfqueue_rule_new, *nfqueue_rule_established, *nfqueue_rule_related, *nfqueue_rule_invalid;

	table = allocate_table(NFPROTO_IPV6, "ipv6y");
	chain_pre = allocate_chain("ipv6y", "prerouting", "nat", NF_INET_PRE_ROUTING, 1);
	chain_post = allocate_chain("ipv6y", "postrouting", "nat", NF_INET_POST_ROUTING, 1);
	chain_forward = allocate_chain("ipv6y", "forward_nfqueue", "filter", NF_INET_FORWARD, 1);
	nfqueue_rule_new = allocate_nfqueue_rules("ipv6y", "forward_nfqueue", interface, 0, NFT_CT_NEW);
	nfqueue_rule_established = allocate_nfqueue_rules("ipv6y", "forward_nfqueue", interface, 1, NFT_CT_ESTABLISHED);
	nfqueue_rule_related = allocate_nfqueue_rules("ipv6y", "forward_nfqueue", interface, 2, NFT_CT_RELATED);
	nfqueue_rule_invalid = allocate_nfqueue_rules("ipv6y", "forward_nfqueue", interface, 3, NFT_CT_INVALID);
	//TODO: add nfqueue target rules

	start_batch(handle);
	handle->family = nftnl_table_get_u32(table, NFTNL_TABLE_FAMILY);
	struct nlmsghdr* nlh = nftnl_table_nlmsg_build_hdr(mnl_nlmsg_batch_current(handle->batch),
						NFT_MSG_NEWTABLE, handle->family,
						NLM_F_CREATE|NLM_F_ACK, handle->seq++);
	nftnl_table_nlmsg_build_payload(nlh, table);
	nftnl_table_free(table);
	mnl_nlmsg_batch_next(handle->batch);

	add_chain_to_batch(handle, chain_pre);
	add_chain_to_batch(handle, chain_post);
	add_chain_to_batch(handle, chain_forward);
	add_rule_to_batch(handle, nfqueue_rule_new, NFT_MSG_NEWRULE);
	//add_rule_to_batch(handle, nfqueue_rule_established, NFT_MSG_NEWRULE);
	//add_rule_to_batch(handle, nfqueue_rule_related, NFT_MSG_NEWRULE);
	//add_rule_to_batch(handle, nfqueue_rule_invalid, NFT_MSG_NEWRULE);

	end_batch(handle);
	send_data(handle, NULL);
}

struct nftnl_handle* init(uint32_t interface){
	struct nftnl_handle* handle = allocate_handle();

	create_default_table_chains(handle, interface);

	printf("Initialized nftnl_helper!\n");

	return handle;
}
