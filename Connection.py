##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

from ipaddress import IPv6Address
from dpkt import ip

class Connection:
	"""
	Contains all necessary information about a connection
	"""
	def __init__(self, packet = None, source = 0, destination = 0, sport = 0, dport = 0, protocol = ip.IP_PROTO_TCP):
		"""
		@param packet: packet data from dpkt. Dont specify any other parameter if using this.
		@param source: source address
		@param destination: destination address
		@param sport: source port
		@param dport: destination port
		@param protocol: protocol index default: p.IP_PROTO_TCP
		"""
		if packet is not None:
			source = packet.src
			destination = packet.dst
			protocol = packet.p
			if protocol == ip.IP_PROTO_TCP:
				sport = packet.tcp.sport
				dport = packet.tcp.dport
			if protocol == ip.IP_PROTO_UDP:
				sport = packet.udp.sport
				dport = packet.udp.dport

		self.protocol = protocol
		self.source = IPv6Address(source)
		self.destination = IPv6Address(destination)
		self.sport = sport
		self.dport = dport

	def __eq__(self, other):
		return (
			self.protocol == other.protocol
			and self.source == other.source
			and self.sport == other.sport
			and self.destination == other.destination
			and self.dport == other.dport
		)

	def __hash__(self):
		return self.__str__().__hash__()

	def __str__(self):
		return '%s connection: %s -> %s' % (
			protocol_str(self.protocol),
			format_address(self.source, self.sport),
			format_address(self.destination, self.dport)
		)

	def is_tcp(self):
		"""
		@return true if the connection protocol is tcp
		"""
		return self.protocol == ip.IP_PROTO_TCP

def protocol_str(protocol):
	"""
	@return the corresponding string for the given protocol index. none if not found
	"""
	if protocol == ip.IP_PROTO_TCP:
		return 'tcp'
	if protocol == ip.IP_PROTO_UDP:
		return 'udp'
	if protocol == ip.IP_PROTO_ICMP6:
		return 'icmpv6'
	return None

def format_address(addr, port=None):
	if port:
		return '[%s]:%s' % (format_address(addr), port)
	return addr.compressed
