#!/bin/bash -ex

WLAN_INTERFACE=$(iw dev | grep Interface | cut -d " " -f 2)
LAN_INTERFACE=eth0
WLAN_IPV6_PREFIX=fd11::
LOCALE="de_DE.UTF-8"
WLAN_PASSWORD="unsicher"


##### Config end ######

apt-get update && apt-get install -y dnsmasq hostapd cmake git build-essential python3-dev python3-pip python3-venv iw locales libnetfilter-conntrack-dev libnetfilter-queue-dev libnfnetlink-dev libcap-dev nftables libnftnl-dev


cat <<EOF > /etc/locale.gen
${LOCALE} UTF-8

EOF

cat <<EOF > /etc/default/locale
LANG=${LOCALE}

EOF

#don't leak ipv4 traffic
#TODO: proper persistent rules
iptables -P FORWARD DROP
#iptables -A INPUT -i ${LAN_INTERFACE} -j DROP

locale-gen

cat <<EOF > /etc/network/interfaces.d/wlan

allow-hotplug wlan0
iface ${WLAN_INTERFACE} inet6 static
	address ${WLAN_IPV6_PREFIX}1
	netmask 48

iface ${WLAN_INTERFACE} inet static
	address 172.16.36.1
	netmask 24

EOF

#TODO: why? /etc/network seems to be ignored (for ipv4)? eth0 isn't configured either. race condition?
cat <<EOF >> /etc/dhcpcd.conf
interface ${WLAN_INTERFACE}
static ip_address=172.16.36.1/24 #This causes ipv6 to leak sometimes

## prefix delegation
noipv6rs  
denyinterfaces ${WLAN_INTERFACE}
interface ${LAN_INTERFACE}
 ipv6rs 
 ia_na 1
 ia_pd 2/::/56 ${LAN_INTERFACE}


EOF

systemctl daemon-reload
systemctl restart dhcpcd.service
systemctl restart networking.service

cat <<EOF > /etc/hostapd/hostapd.conf
interface=${WLAN_INTERFACE}
driver=nl80211
ssid=ipv6y
channel=1

# ESSID sichtbar
ignore_broadcast_ssid=0

# Ländereinstellungen
country_code=DE
ieee80211d=1

# draft-n
ieee80211n=1

# Übertragungsmodus
hw_mode=g

# 40mhz kanalbreite
#ht_capab=[HT40+][SHORT-GI-40][DSSS_CCK-40]

# MAC-Authentifizierung
macaddr_acl=0

# wmm-Funktionalität
wmm_enabled=0

# WLAN-Verschlüsselung
auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase=${WLAN_PASSWORD}

EOF

chmod 600 /etc/hostapd/hostapd.conf

cat <<EOF > /etc/default/hostapd
DAEMON_CONF="/etc/hostapd/hostapd.conf"

EOF

systemctl enable hostapd
systemctl start hostapd

cat <<EOF > /etc/sysctl.d/00-ipv6-router.conf
net.ipv6.conf.all.forwarding=1

EOF

sysctl --system

#### dnsmasq
# TODO: change to radvd?

cat <<EOF > /etc/dnsmasq.conf
interface=${WLAN_INTERFACE}

enable-ra
ra-param=high,60
dhcp-range=tag:${WLAN_INTERFACE}, ::, constructor:${WLAN_INTERFACE}, ra-stateless

#android won't connect without ipv4
dhcp-range=lan,172.16.36.64,172.16.36.127,12h

EOF

systemctl restart dnsmasq

cd && git clone https://gitlab.com/ipv6y/ipv6y

cd ipv6y 

./scripts/setup_venv.sh ./venv

mkdir build && cd build && cmake .. && make && make install

printf "-----------------------------\n"
printf "Setup done. You may need to install a new kernel.\n To start ipv6 run the following:\nsource ./venv/bin/activate\n./ipv6y -i <interface> -g -m dest"


#get ipv6
#curl -6 ifconfig.k0nsl.org

