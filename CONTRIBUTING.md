Merge requests are highly welcome.

In contrast to the suggestion in PEP 8, we are using tabs for intendation.
Using tabs has only advantages and prevents syntax errors due to wrong intendation.

Other than that we don't have any coding guidelines.
If you add any new functionality (such as new bouquets), please provide test cases.