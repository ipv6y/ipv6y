project(ipv6y)

cmake_minimum_required(VERSION 3.6)

add_subdirectory(libnftnl_helper)
add_subdirectory(libconntrack_helper)
