#!/usr/bin/env python3
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

import sys
import os
import signal
import threading
import time
from argparse import ArgumentParser, RawTextHelpFormatter
from subprocess import Popen, PIPE

import deescalate

from ipaddress import IPv6Interface
from dpkt import ip6
from netfilterqueue import NetfilterQueue

# Local imports
import log
from Connection import Connection
from ConnectionManager import ConnectionManager
from Bouquet import (Bouquet_connection,
					Bouquet_time,
					Bouquet_destination,
					Bouquet_destination_time,
					Bouquet_destination_sld)
from AddressGenerator import AddressGenerator_random
import NetworkHelper

from libconntrack_helper.python import conntrack_helper

class Ipv6y:
	"""
	This class is the heart of ipv6y.
	It manages incoming signals and creates the nfqueues and passes incoming packages to the \ref{ConnectionManager}
	"""
	def __init__(self, connectionManager):
		self.connectionManager = connectionManager
		self.nfqueue_new = NetfilterQueue()

		def func():
			self.checkConntrack()
			time.sleep(10)
		self.check_conntrack_thread = threading.Thread(target=func)
		#self.nfqueue_established = NetfilterQueue()
		self.running = True

		signal.signal(signal.SIGUSR1, self.__receive_signal)
		signal.signal(signal.SIGUSR2, self.__receive_signal)

		self.nfqueue_new.bind(0, self.handlePayloadNew)
		#self.nfqueue_established.bind(1, self.handlePayloadEstablished)
		conntrack_helper.start()

	def __del__(self):
		self.nfqueue_new.unbind()
		conntrack_helper.stop()

	def __receive_signal(self, signum, stack):
		"""
		Sending USR1 will print stats about total opened and closed connections.
		"""
		if signum == 10: #USR1
			opened, closed, curr_open = self.connectionManager.getStats()
			print("Opened connections %i, Closed connections %i, Open connections %i" % (opened, closed, curr_open))

	def start(self):
		"""
		Start the conntrack thread
		@deprecated: will be removed
		"""
		self.check_conntrack_thread.start()

	def run(self, blocking=True):
		"""
		Start the ipv6y service
		@param blocking if true this function will block
		@attention blocking does nothing
		"""
		try:
			#TODO: add support for multiple queues using threads
			self.nfqueue_new.run(True)
		except KeyboardInterrupt:
			log.warning('Interrupted. Shutting down...')
			self.running = False
			self.nfqueue_new.unbind()
			time.sleep(5)

	def checkConntrack(self):
		"""
		Checks conntrack for new events and calls @ref onClosedConnection for every event
		@deprecated: will be removed
		"""
		if conntrack_helper.isAvailable():
			info = conntrack_helper.getConnectionInfo()

			src = conntrack_helper.to128(info.src)
			dst = conntrack_helper.to128(info.dst)

			connection = Connection(source = src, destination = dst, sport = info.sport, dport = info.dport, protocol = info.l4proto)

			#TODO: check for state?
			self.onClosedConnection(connection)

	def handlePayloadNew(self, payload):
		"""
		Handles a payload belonging to a new connection
		@param payload: the payload given by nfqueue
		"""

		data = payload.get_payload()
		packet = ip6.IP6(data)

		connection = Connection(packet)

		log.info("Handling new %s" % connection)

		self.onNewConnection(connection)

		payload.accept()

	def handlePayloadEstablished(self, payload):
		"""
		Handles a payload belonging to an established connection
		@param payload: the payload given by nfqueue
		"""
		payload.accept()

	def onNewConnection(self, connection):
		"""
		Will be called when a new connection is opened
		"""
		self.connectionManager.addConnection(connection)

	def onEstablishedConnection(self, connection):
		pass

	def onClosedConnection(self, connection):
		"""
		Will be called by conntrack, when a connection is closed
		@deprecated: will be removed
		"""
		log.info("Conntrack detected a closed %s" % connection)
		self.connectionManager.removeConnection(connection)

def drop_capabilities():
	"""
	Drop almost all capabilities and change the user
	"""
	try:
		deescalate.lockdown_account(uid='nobody', gid='nogroup', caps_to_keep='net_admin')
	except:
		log.error("Failed to drop capabilities...")

def main(argv=None):
	'''Command line options.'''
	'''detect possible address space for prefix bouquets'''
	process = Popen(["/usr/sbin/nft", "delete", "table", "ip6", "ipv6y"], stdout=PIPE)
	process.communicate()

	drop_capabilities()

	#prefixes = NetworkHelper.getPrefixDelegations()
	#preflen = prefixes[0].prefixlen
	#prefrangelist=list(range(128-preflen))

	if argv is None:
		argv = sys.argv
	else:
		sys.argv.extend(argv)

	# Setup argument parser
	parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
	parser.add_argument("-i", "--interface", dest="interface",
						help="Set interface to masquerade [default: %(default)s]")
	parser.add_argument("-b", "--blacklist", dest="blacklist", type=str,
						help="Blacklist IPs for masquerading",
						nargs='+')
	parser.add_argument("-g", "--global-only", dest="onlyGlobal", action="store_true",
						help="Only use global prefixes [default: %(default)s]")
	parser.add_argument("-m", "--mode", dest="mode", type=str, choices=['conn', 'dest', 'dest-sld'], default='conn',
						help="sets the pattern in which the prefix is changed:\n"
						"    conn - connection based\n"
						"    dest - destination based\n"
						"    dest-sld - based on second level domain of destination\n"
						)
	parser.add_argument("-t", "--time", dest="interval", type=int,
						help="sets interval in seconds for changing prefixes. Min and max values for random interval possible.",
						nargs='+')

	parser.add_argument("-p", "--prefixes", dest="prefixes", type=str,
						help="add prefixes manually",
						nargs='+')
	#parser.add_argument("-r", "--range", dest="prefrange", type=int, choices=prefrangelist, default=0, help="sets address range for prefix bouquet")

	# Process arguments
	args = parser.parse_args()

	interface = args.interface
	blacklist = args.blacklist
	onlyGlobal = args.onlyGlobal
	mode = args.mode
	interval = args.interval
	prefixes_str = args.prefixes
	prefrange = 0

	if interval is None:
		interval = [0]

	if interface is None:
		log.error("Error: No interface set!")
		parser.print_help()
		return 1

	if blacklist is None:
		blacklist = []

	if (prefixes_str is None) and (prefrange < 1):
		prefixes = NetworkHelper.getPrefixDelegations(onlyGlobal, blacklist)
	elif (prefixes_str is None) and (prefrange >= 1):
		prefixes = NetworkHelper.getPrefixDelegations(onlyGlobal, blacklist)
		subnets = list(prefixes[0].subnets(prefixlen_diff=prefrange))
		prefixes = subnets
	else:
		prefixes = []
		for prefix in prefixes_str:
			prefixes.append(IPv6Interface(prefix).network)

	log.info("Available prefixes: %s" % prefixes)

	if len(prefixes) == 0:
		log.error("No prefixes available. Aborting!")
		os._exit(1)

	addressGenerator = AddressGenerator_random(prefixes)

	if mode == 'conn' and max(interval) <= 0:
		bouquet = Bouquet_connection(addressGenerator)
	elif mode == 'dest' and max(interval) <= 0:
		bouquet = Bouquet_destination(addressGenerator)
	elif mode == 'dest':
		bouquet = Bouquet_destination_time(addressGenerator, min(interval), max(interval))
	elif mode == 'dest-sld':
		bouquet = Bouquet_destination_sld(addressGenerator)
	elif max(interval) > 0:
		bouquet = Bouquet_time(addressGenerator, min(interval), max(interval))
	else:
		log.error('prefix change pattern not recognized.')
		parser.print_help()
		os._exit(1)

	connectionManager = ConnectionManager(bouquet, NetworkHelper.interfacename_to_id(interface))

	ipv6y = Ipv6y(connectionManager)
	ipv6y.start()
	ipv6y.run()

if __name__ == '__main__':
	main()
