#!/bin/sh -x
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

remove_venv () {
	rm -rf ${VENV_PATH}
	exit 1
}

PARAM1=$1

if [ -z "$1" ]; then
	PARAM1=./venv
fi

if [ -d ${PARAM1} ]; then
        echo "dir ${VENV_PATH} already exists. Assuming this script already ran."
        exit 0
fi

mkdir -p $PARAM1
VENV_PATH=$(readlink -f $PARAM1)

PYTHON_BIN=$VENV_PATH/bin/python3
PIP_BIN=$VENV_PATH/bin/pip
PY_NFQUEUE_URL=https://gitlab.com/ipv6y/python-netfilterqueue
DEESCALATE_URL=https://github.com/Smeat/deescalate

python3 -m venv "$VENV_PATH"

$PIP_BIN install --upgrade wheel || remove_venv
$PIP_BIN install --upgrade dpkt coverage tldextract pyblake2 || remove_venv

git clone $PY_NFQUEUE_URL py-nfqueue ; cd py-nfqueue && git pull && $PYTHON_BIN ./setup.py install || remove_venv ; cd .. && rm -rf ./py-nfqueue

git clone $DEESCALATE_URL deescalate ; cd deescalate && git pull && $PYTHON_BIN ./setup.py install || remove_venv ; cd .. && rm -rf deescalate

