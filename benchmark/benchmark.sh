#!/bin/bash -x
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##

#TODO: only use one connection. tmux on local pc with an open ssh session?
#TODO: getopt with double dash names (--file=XXX)

SSH_HOST=root@fd11::1
SSH_FOLDER=/tmp

IPV6Y_BASE_PARAMETERS="-g -i eth0"
IPV6Y_PARAMETERS=("-m dest" "-m conn")

GIT_REPO="git@git.informatik.uni-hamburg.de:0koester/ipv6y.git"

TESTING_COMMITS=("iptables" "iptables_restore" "iptables_python" "nftables")
ENABLED_BENCHMARKS=(siege)

# General benchmark config
BENCHMARK_GENERAL_CONNECTIONS=(1 10 50 100 250)
BENCHMARK_GENERAL_WAIT_TIME=60
BENCHMARK_GENERAL_TIME=60

# siege benchmark config
BENCHMARK_SIEGE_BIN=/home/kevin/siege/siege-src/src/siege
BENCHMARK_SIEGE_BASE_DIR=/tmp/bench/
BENCHMARK_SIEGE_RC_FILE=${BENCHMARK_SIEGE_BASE_DIR}/siegerc
BENCHMARK_SIEGE_URL_FILE=${BENCHMARK_SIEGE_BASE_DIR}/urls
BENCHMARK_SIEGE_PARAMETERS="-R ${BENCHMARK_SIEGE_RC_FILE} -f ${BENCHMARK_SIEGE_URL_FILE}"
BENCHMARK_SIEGE_LOG_FILE=${BENCHMARK_SIEGE_BASE_DIR}/siege.log


# iperf benchmark config
BENCHMARK_IPERF3_TESTSERVER=fdcb:2f08:d7c6:10:94c8:e6fc:3e8a:e314         # insert Testserver URL here
BENCHMARK_IPERF3_INTERVAL=60         # decrease to get reports in intervals
BENCHMARK_IPERF3_LOGFILE=/tmp/iperf3.log



####### END OF CONFIG ########

TOTAL_NUM_PARAMETERS=$((${#TESTING_COMMITS[@]} * ${#IPV6Y_PARAMETERS[@]}))


function clone_repo(){
	ssh ${SSH_HOST} "cd ${SSH_FOLDER} && rm -rf ipv6y && git clone ${GIT_REPO}"
}

function checkout_branch(){
	BRANCH=$1
	ssh ${SSH_HOST} "cd ${SSH_FOLDER}/ipv6y && git checkout ${BRANCH} && git reset --hard && git clean -x -d -f && ./scripts/setup_venv.sh ./venv && mkdir libconntrack_helper/build && cd libconntrack_helper/build && cmake .. && make install && cd ../.. && if [ -d libnftnl_helper ]; then mkdir libnftnl_helper/build && cd libnftnl_helper/build && cmake .. && make install && cd ../..; fi"
}

function start_ipv6y(){
	PARAM=$1
	ssh ${SSH_HOST} "cd ${SSH_FOLDER}/ipv6y && tmux kill-session -t ipv6y ; tmux new -s ipv6y -d './venv/bin/python3 ./ipv6y.py ${PARAM} >> /tmp/ipv6y.log 2>>/tmp/ipv6y.err'"
}

function stop_ipv6y(){
	ssh ${SSH_HOST} 'tmux kill-session -t ipv6y'
}

function start_benchmarks(){
	NAME=$1
	IPV6Y_PARAM=$2
	for i in "${ENABLED_BENCHMARKS[@]}"; do
		start_benchmark_$i "$NAME" "$IPV6Y_PARAM"
	done
}

function start_benchmark_siege(){
	#echo "$1_$2" >> ${BENCHMARK_SIEGE_LOG_FILE}
	for concurrent in "${BENCHMARK_GENERAL_CONNECTIONS[@]}"; do
		echo -n "$1_$2, $concurrent, " >> ${BENCHMARK_SIEGE_LOG_FILE}
		SIEGE_OUTPUT=$(${BENCHMARK_SIEGE_BIN} ${BENCHMARK_SIEGE_PARAMETERS} -c $concurrent)
		echo "Waiting $BENCHMARK_GENERAL_WAIT_TIME sec for next benchmark..."
		sleep $BENCHMARK_GENERAL_WAIT_TIME
	done
}

function start_benchmark_iperf3_tcp(){
	for concurrent in "${BENCHMARK_GENERAL_CONNECTIONS[@]}"; do
		iperf3 -V -6 -i ${BENCHMARK_IPERF3_INTERVAL} -c ${BENCHMARK_IPERF3_TESTSERVER} -t ${BENCHMARK_GENERAL_TIME} --logfile "${BENCHMARK_IPERF3_LOGFILE}_tcp_$1_$2_$concurrent" -P $concurrent
		echo "Waiting $BENCHMARK_GENERAL_WAIT_TIME sec for next benchmark..."
		sleep $BENCHMARK_GENERAL_WAIT_TIME
	done
}


function start_benchmark_iperf3_udp(){
	for concurrent in "${BENCHMARK_GENERAL_CONNECTIONS[@]}"; do
		iperf3 -V -6 -i ${BENCHMARK_IPERF3_INTERVAL} -c ${BENCHMARK_IPERF3_TESTSERVER} -t ${BENCHMARK_GENERAL_TIME} --logfile "${BENCHMARK_IPERF3_LOGFILE}_udp_$1_$2_$concurrent" -P $concurrent -u
		echo "Waiting $BENCHMARK_GENERAL_WAIT_TIME sec for next benchmark..."
		sleep $BENCHMARK_GENERAL_WAIT_TIME
	done
}

#TODO: don't depend on script variables
function render_benchmark_siege(){
	#TODO: fix labels
	sort -t, -k2 -n ${BENCHMARK_SIEGE_LOG_FILE}
	
	TITLE_ARRAY="array conn[${#BENCHMARK_GENERAL_CONNECTIONS[@]}];"
	
	for conn in $(seq 1 ${#BENCHMARK_GENERAL_CONNECTIONS[@]}); do
		TITLE_ARRAY="$TITLE_ARRAY conn[$conn] = ${BENCHMARK_GENERAL_CONNECTIONS[$conn-1]};"
	done
	
	gnuplot -e "set datafile separator ',';
		set xlabel 'Options';
		set ylabel 'Hits';
		set term png;
		set output 'data.png';
		set style fill solid 0.5;
		set boxwidth 0.5 relative;
		set xtic rotate by -45 scale 0;
		set style data histogram;
		set style histogram cluster gap 1;
		set style fill solid border -1;
		$TITLE_ARRAY
		plot for[i=1:${#BENCHMARK_GENERAL_CONNECTIONS[@]}] \"<(sort -t, -k2 -n ${BENCHMARK_SIEGE_LOG_FILE})\" using 4:xtic(1) every ::(i - 1)*${TOTAL_NUM_PARAMETERS}+1::i*${TOTAL_NUM_PARAMETERS} with histogram title sprintf(\"%i Verb.\", conn[i]);"
}

function test_commits(){
	clone_repo
	for commit in "${TESTING_COMMITS[@]}"; do
		checkout_branch "$commit"
		for ipv6y_parameter in "${IPV6Y_PARAMETERS[@]}"; do
			start_ipv6y "$IPV6Y_BASE_PARAMETERS $ipv6y_parameter"
			sleep 10
			start_benchmarks "$commit" "$ipv6y_parameter"
			stop_ipv6y
			sleep 5
		done
	done
}

function render_benchmarks(){
	echo "Rendering benchmark data..."
	for benchmark in "${ENABLED_BENCHMARKS[@]}"; do
		render_benchmark_${benchmark}
	done
}

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")

while getopts ":art:" opt; do
	case $opt in
		a)
			echo "Running all tests..." >&2
			test_commits
			render_benchmarks
			;;

		r)
			render_benchmarks
			;;

		t)
			BENCHMARK_GENERAL_WAIT_TIME=$OPTARG
			;;

		\?)
			echo "Invalid option: -$OPTARG" >&2
			usage
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			exit 1
			;;
	esac
done
