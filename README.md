Master

[![build status](https://gitlab.com/ipv6y/ipv6y/badges/master/pipeline.svg)](https://gitlab.com/ipv6y/ipv6y/commits/master)
[![coverage report](https://gitlab.com/ipv6y/ipv6y/badges/master/coverage.svg)](https://gitlab.com/ipv6y/ipv6y/commits/master)

Dev

[![build status](https://gitlab.com/ipv6y/ipv6y/badges/dev/pipeline.svg)](https://gitlab.com/ipv6y/ipv6y/commits/dev)
[![coverage report](https://gitlab.com/ipv6y/ipv6y/badges/dev/coverage.svg)](https://gitlab.com/ipv6y/ipv6y/commits/dev)


# IPv6y

The aim of `ipv6y` is to anonymize network traffic using multiple ipv6 addresses and prefixes. 
We use a Raspberry Pi, which opens a WLAN. The traffic for any client connected will be anonymized on a network level.

## Requirements
To use ipv6y you your kernel needs to support `nftables` (Raspbian stretch doesn't). In some cases you need to recompile the kernel.
Required kernel options (Some of them may not be needed):

```
CONFIG_NETFILTER_NETLINK=y
CONFIG_NF_TABLES=y
CONFIG_NF_TABLES_INET=m
CONFIG_NF_TABLES_NETDEV=m
CONFIG_NFT_EXTHDR=m
CONFIG_NFT_META=m
CONFIG_NFT_NUMGEN=m
CONFIG_NFT_CT=m
CONFIG_NFT_SET_RBTREE=m
CONFIG_NFT_SET_HASH=m
CONFIG_NFT_COUNTER=m
CONFIG_NFT_LOG=m
CONFIG_NFT_LIMIT=m
CONFIG_NFT_MASQ=m
CONFIG_NFT_REDIR=m
CONFIG_NFT_NAT=m
CONFIG_NFT_QUEUE=m
CONFIG_NFT_QUOTA=m
CONFIG_NFT_REJECT=m
CONFIG_NFT_REJECT_INET=m
CONFIG_NFT_COMPAT=m
CONFIG_NFT_HASH=m
CONFIG_NF_DUP_NETDEV=m
CONFIG_NFT_DUP_NETDEV=m
CONFIG_NFT_FWD_NETDEV=m

CONFIG_NF_TABLES_IPV4=m
CONFIG_NFT_REJECT_IPV4=m

CONFIG_NF_TABLES_IPV6=m
CONFIG_NFT_REJECT_IPV6=m

CONFIG_NFT_CHAIN_NAT_IPV6=m

CONFIG_NFT_MASQ_IPV6=m
CONFIG_NFT_REDIR_IPV6=m

```
The complete config we used can be found at https://gitlab.com/ipv6y/ipv6y/snippets/1696820

Compiling kernel for raspberry pi: https://www.raspberrypi.org/documentation/linux/kernel/building.md

### Packages
The following packages are necessary to run ipv6y on debian
```
python3.4 libnfnetlink-dev libnetfilter-queue-dev nftables libnftnl-dev libcap-dev
```

If you have a fresh Raspbian installation you can also run the script in `scripts/setup_debian.sh`, which will install and configure everything (WLAN, DHCP client and server etc).

### python packages

Use ```scripts/setup_venv.sh``` to create a working python environment (virtualenv required).
After installation use ```source ./venv/bin/activate``` to use the virtual environment.

## Installation

```
mkdir build && cd build && cmake .. && make && make install
```

## System

The system needs a delegated prefix on the outgoing interface and an ULA-prefix on the inner (W)LAN-Interface.

Some example configurations:

dhcpcd.conf
```
noipv6rs
denyinterfaces ${WLAN_INTERFACE}
interface ${LAN_INTERFACE}
 ipv6rs
 ia_na 1
 ia_pd 2/::/56 ${LAN_INTERFACE}
```
dnsmasq.conf
```
interface=${WLAN_INTERFACE}
enable-ra
ra-param=high,60
dhcp-range=tag:${WLAN_INTERFACE}, ::, constructor:${WLAN_INTERFACE}, ra-stateless
```

## Usage

```
usage: ipv6y.py [-h] [-i INTERFACE] [-b BLACKLIST [BLACKLIST ...]] [-g]
                [-m {conn,dest,dest-sld}] [-t INTERVAL [INTERVAL ...]]
                [-p PREFIXES [PREFIXES ...]] [-r {0,1,2,3,4,5,6}]

optional arguments:
  -h, --help            show this help message and exit
  -i INTERFACE, --interface INTERFACE
                        Set interface to masquerade [default: None]
  -b BLACKLIST [BLACKLIST ...], --blacklist BLACKLIST [BLACKLIST ...]
                        Blacklist IPs for masquerading
  -g, --global-only     Only use global prefixes [default: False]
  -m {conn,dest,dest-sld}, --mode {conn,dest,dest-sld}
                        sets the pattern in which the prefix is changed:
                            conn - connection based
                            dest - destination based
                            dest-sld - based on second level domain of destination
  -t INTERVAL [INTERVAL ...], --time INTERVAL [INTERVAL ...]
                        sets interval in seconds for changing prefixes. Min and max values for random interval possible.
  -p PREFIXES [PREFIXES ...], --prefixes PREFIXES [PREFIXES ...]
                        add prefixes manually
  -r {0,1,2,3,4,5,6}, --range {0,1,2,3,4,5,6}
                        sets address range for prefix bouquet
```
## Running with restricted permissions

### Firejail
In order to restrict the permissions of `ipv6y` install firejail either using your package manager or using the script in `scripts/install_firejail.sh`.

Then run `firejail --profile=./ipv6y.py.profile ./ipv6y.py <options>`

If you get the error `unable to initialize table 'filter'`, you need to load all necessary modules since the process won't have the permissions to do so.

WARNING:
This is currently not tested and firejail might not the most secure option.

### Capabilities
`ipv6y` will change the user to `nobody` with the group `nogroup` and drop all Capabilities except `net_admin`.

# Documentation

To build the documentation you need doxygen and doxypypy installed and an executable file named "py_filter" in your path containing
```
#!/bin/bash
doxypypy -a -c $1
```

The current version of the decumentation can be found at https://ipv6y.gitlab.io/ipv6y/

# TODOs / Bugs

* Remove the current ugly workaround to detect prefixes (this is different on every distribution)
* Investigate problems on android (won't connect if no IPv4-address is given)
* Improve speed and reliability
* Test different bouquets
