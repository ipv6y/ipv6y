##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-
from ctypes import cdll, c_uint32, c_uint16, POINTER, c_char_p, c_uint64, c_void_p, byref

from ipaddress import IPv6Address
import os
import socket
path = os.path.dirname(os.path.realpath(__file__))

library = cdll.LoadLibrary(path + "/libnftnl_helper.so")

#class nftnl_handle(Structure):
 #   _fields_ = (
  #	  ("buf", c_char_p ),
  #	  ("buf_size", c_uint32),
  #	  ("seq", c_uint32),
  #	  ("family", c_uint32),
  #	  ("batching", c_uint32),
  #	  ("batch", c_void_p)
  #  )
#nftnl_handle_p = POINTER(nftnl_handle)

add_nat_pair_c = library.add_nat_pair
add_nat_pair_c.argtypes = [c_void_p, c_char_p, c_uint32, c_uint32 * 4, c_uint32 * 4, c_uint32 *4, c_uint16, c_uint16, c_uint32, POINTER(c_uint64), POINTER(c_uint64)]

rule_del_c = library.rule_del
rule_del_c.argtypes = [c_void_p, c_char_p, c_char_p, c_uint64]

init_c = library.init
init_c.restypes = c_void_p

def addr_to_array(addr):
	address = IPv6Address(addr)
	array = (c_uint32 * 4)()

	array[3] = socket.ntohl((int(address) >> 0) & 0xFFFFFFFF)
	array[2] = socket.ntohl((int(address) >> 32) & 0xFFFFFFFF)
	array[1] = socket.ntohl((int(address) >> 64) & 0xFFFFFFFF)
	array[0] = socket.ntohl((int(address) >> 96) & 0xFFFFFFFF)

	return array

class nftnl_helper:
	def __init__(self, interface_num):
		self.handle = c_void_p(init_c(interface_num))
		self.interface = interface_num

	def rule_del(self, table_name, chain_name, handle_num):
		rule_del_c(self.handle, c_char_p(table_name), c_char_p(chain_name), handle_num)

	def add_nat_pair(self, table_name, protocol, source, destination, addr, sport, dport):
		snat_handle = c_uint64()
		dnat_handle = c_uint64()
		source_array = addr_to_array(source)
		destination_array = addr_to_array(destination)
		addr_array = addr_to_array(addr)

		add_nat_pair_c(self.handle, c_char_p(table_name), protocol, source_array, destination_array,
					addr_array, sport, dport, self.interface, byref(snat_handle), byref(dnat_handle))

		return(snat_handle.value, dnat_handle.value)

