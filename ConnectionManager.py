##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-

from libnftnl_helper.python import nftnl_helper
import log

class ConnectionManager:
	"""
	This class manages all new connections. For every new connection an outgoing and an incoming rule is added to nftables
	"""
	def __init__(self, bouquet, interface):
		self.connectionMap = {}
		self.bouquet = bouquet
		self.nft = nftnl_helper.nftnl_helper(interface)
		self.sumNewConnections = 0
		self.sumClosedConnections = 0

	def __del__(self):
		# TODO: Delete our table
		pass

	def addConnection(self, connection):
		"""
		Adds a new connection, gets a new address from the given bouquet and adds NAT rules
		@param connection: the connection to add
		@todo: How to handle connections we already know and are marked as new?
		"""
		if connection not in self.connectionMap:
			self.sumNewConnections += 1
			address = self.bouquet.getAddress(connection)
			snat_handle, dnat_handle = self._addNAT(connection, address)
			self.connectionMap[connection] = (address, snat_handle, dnat_handle)

	def removeConnection(self, connection):
		"""
		Removes the given connection from the connectionmap and clears the corresponding NAT rules.
		@param connection: the connection to remove
		"""
		if connection in self.connectionMap:
			self.sumClosedConnections += 1
			log.info("Removing connection %s" % connection)
			address, snat_handle, dnat_handle = self.connectionMap[connection]
			self._removeNAT(snat_handle, dnat_handle)
			del self.connectionMap[connection]

	def _addNAT(self, connection, address):
		return self.nft.add_nat_pair(b"ipv6y", connection.protocol, connection.source,
									connection.destination,address, connection.sport, connection.dport)

	def _removeNAT(self, snat_handle, dnat_handle):
		self.nft.rule_del(b"ipv6y", b"prerouting", dnat_handle)
		self.nft.rule_del(b"ipv6y", b"postrouting", snat_handle)

	def getStats(self):
		"""
		Get the current stats for openend, closed and currently active connections
		@return: A 3-tuple containning the (total amount of new connections,
		total amount of closed connections, open connections)
		"""
		return (self.sumNewConnections, self.sumClosedConnections, len(self.connectionMap))
