#!/usr/bin/env python3
##
## This file is part of ipv6y 
## 
## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU Affero General Public License as
## published by the Free Software Foundation, either version 3 of the
## License, or (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU Affero General Public License for more details.
## 
## You should have received a copy of the GNU Affero General Public License
## along with this program. If not, see <http://www.gnu.org/licenses/>.
##
# -*- coding: utf-8 -*-
"""
This is the unittest for most of ipv6y.
"""

import unittest

import os
import time

from ipaddress import IPv6Address, IPv6Network
from Connection import Connection, protocol_str
from libnftnl_helper.python import nftnl_helper
from ConnectionManager import ConnectionManager

from AddressGenerator import AddressGenerator_random
from Bouquet import (Bouquet,
					Bouquet_connection,
					Bouquet_time,
					Bouquet_destination,
					Bouquet_destination_time,
					Bouquet_destination_sld)

from subprocess import Popen, PIPE

@unittest.skipIf(os.getuid() != 0 or os.geteuid() != 0, "Need too be root to test this")
class TestConnectionManager(unittest.TestCase):
	def setUp(self):
		prefixes = [IPv6Network("2001:db8::/32")]
		self.addressGenerator = AddressGenerator_random(prefixes)
		self.bouquet = Bouquet_connection(self.addressGenerator)
		self.connectionManager = ConnectionManager(self.bouquet, 2)

		self.src_str = "2001:db08::1"
		self.dst_str = "2001:db08::2"
		self.src_addr = IPv6Address(self.src_str)
		self.dst_addr = IPv6Address(self.dst_str)
		self.dport = 80
		self.sport = 5001
		self.protocol = 6

		self.dst2_str = "2001:db08::3"
		self.dst2_addr = IPv6Address(self.dst2_str)

		self.conn1 = Connection(source=self.src_addr, destination=self.dst_addr, sport=self.sport, dport=self.dport, protocol=self.protocol)
		self.conn2 = Connection(source=self.src_addr, destination=self.dst2_addr, sport=self.sport, dport=self.dport, protocol=self.protocol)

	def test_addConnection(self):
		self.connectionManager.addConnection(self.conn1)
		self.assertEqual(len(self.connectionManager.connectionMap), 1)
		self.connectionManager.addConnection(self.conn1)
		self.assertEqual(len(self.connectionManager.connectionMap), 1)
		self.connectionManager.addConnection(self.conn2)
		self.assertEqual(len(self.connectionManager.connectionMap), 2)

		#TODO: more tests. Test interface for address, iptables etc
		self.connectionManager.removeConnection(self.conn1)

		self.assertEqual(len(self.connectionManager.connectionMap), 1)

		self.connectionManager.removeConnection(self.conn1)

		self.assertEqual(len(self.connectionManager.connectionMap), 1)

		self.connectionManager.removeConnection(self.conn2)

		self.assertEqual(len(self.connectionManager.connectionMap), 0)
		
	def tearDown(self):
		self.connectionManager.__del__()

class TestBouquets(unittest.TestCase):

	def setUp(self):
		prefixes = [IPv6Network("2001:db8::/32")]
		self.addressGenerator = AddressGenerator_random(prefixes)

		self.src_str = "2001:db08::1"
		self.dst_str = "2001:db08::2"
		self.src_addr = IPv6Address(self.src_str)
		self.dst_addr = IPv6Address(self.dst_str)
		self.dport = 5001
		self.sport = 1345
		self.protocol = 6

		self.dst2_str = "2001:db08::3"
		self.dst2_addr = IPv6Address(self.dst2_str)

		self.conn1 = Connection(source=self.src_addr, destination=self.dst_addr, sport=self.sport, dport=self.dport, protocol=self.protocol)
		self.conn2 = Connection(source=self.src_addr, destination=self.dst2_addr, sport=self.sport, dport=self.dport, protocol=self.protocol)

	def test_Base(self):
		bouquet = Bouquet(self.addressGenerator)
		self.assertEqual("", bouquet.getAddress())

	def test_Standard(self):
		bouquet = Bouquet_connection(self.addressGenerator)

		addr1 = bouquet.getAddress(self.conn1)
		addr2 = bouquet.getAddress(self.conn2)

		self.assertNotEqual(addr1, addr2)

	def test_Time(self):
		bouquet = Bouquet_time(self.addressGenerator, 10)

		addr1 = bouquet.getAddress(self.conn1)
		addr2 = bouquet.getAddress(self.conn2)

		self.assertEqual(addr1, addr2)

		time.sleep(15)

		addr3 = bouquet.getAddress(self.conn1)
		self.assertNotEqual(addr1, addr3)
		addr4 = bouquet.getAddress(self.conn1)
		self.assertEqual(addr3, addr4)

	def test_Destination(self):
		bouquet = Bouquet_destination(self.addressGenerator)

		addr1 = bouquet.getAddress(self.conn1)
		addr2 = bouquet.getAddress(self.conn2)
		addr3 = bouquet.getAddress(self.conn1)

		self.assertNotEqual(addr1, addr2)
		self.assertEqual(addr1, addr3)

	def test_Combination(self):
		bouquet = Bouquet_destination_time(self.addressGenerator, 10)

		addr1 = bouquet.getAddress(self.conn1)
		addr2 = bouquet.getAddress(self.conn2)
		addr3 = bouquet.getAddress(self.conn1)

		self.assertNotEqual(addr1, addr2)
		self.assertEqual(addr1, addr3)

		time.sleep(15)

		addr4 = bouquet.getAddress(self.conn1)
		self.assertNotEqual(addr1, addr4)

		addr5 = bouquet.getAddress(self.conn1)
		self.assertEqual(addr4, addr5)

	def test_SLD(self):
		conn_google_com = Connection(source=self.src_addr, destination="2a00:1450:4005:80a::200e", sport=self.sport, dport=80, protocol=self.protocol)
		conn_mail_google_com = Connection(source=self.src_addr, destination="2a00:1450:4005:80a::2005", sport=self.sport, dport=self.dport, protocol=self.protocol)
		conn_heise_de = Connection(source=self.src_addr, destination="2a02:2e0:3fe:1001:302::", sport=self.sport, dport=self.dport, protocol=self.protocol)

		bouquet = Bouquet_destination_sld(self.addressGenerator)

		addr1 = bouquet.getAddress(conn_google_com)
		addr2 = bouquet.getAddress(conn_mail_google_com)
		addr3 = bouquet.getAddress(conn_heise_de)
		addr4 = bouquet.getAddress(self.conn1)

		self.assertEqual(addr1, addr2)
		self.assertNotEqual(addr2, addr3)
		self.assertNotEqual(addr2, addr4)
		self.assertNotEqual(addr3, addr4)

class Testipv6y(unittest.TestCase):

	def setUp(self):
		self.src_str = "2001:db08::1"
		self.dst_str = "2001:db08::2"
		self.src_addr = IPv6Address(self.src_str)
		self.dst_addr = IPv6Address(self.dst_str)
		self.dport = 5001
		self.sport = 1345
		self.protocol = 6

		self.gen_addr = IPv6Address("2001:db08::3")

	def tearDown(self):
		pass

	def test_Connection(self):
		connection_addr = Connection(source=self.src_addr, destination=self.dst_addr, dport=self.dport, sport=self.sport, protocol=self.protocol)
		connection_str = Connection(source=self.src_str, destination=self.dst_str, dport=self.dport, sport=self.sport, protocol=self.protocol)

		self.assertEqual(connection_addr, connection_str)

		self.assertEqual(connection_addr.destination, self.dst_addr)
		self.assertEqual(connection_addr.source, self.src_addr)
		self.assertEqual(connection_addr.dport, self.dport)
		self.assertEqual(connection_addr.sport, self.sport)
		self.assertEqual(connection_addr.protocol, self.protocol)

		connection_test_str = "tcp connection: [2001:db08::1]:1345 -> [2001:db08::2]:5001"

		self.assertEqual(str(connection_addr), connection_addr.__str__())
		self.assertEqual(str(connection_addr), connection_test_str)
		self.assertTrue(connection_addr.is_tcp())

	def test_Connection_helper(self):

		self.assertEqual(protocol_str(6), "tcp")
		self.assertEqual(protocol_str(17), "udp")
		self.assertEqual(protocol_str(58), "icmpv6")
		self.assertEqual(protocol_str("error"), None)
		
@unittest.skipIf(os.getuid() != 0 or os.geteuid() != 0, "Need too be root to test this")
class Testnftables(unittest.TestCase):
		
		def setUp(self):
			process = Popen(["nft", "delete", "table", "ip6", "ipv6y"], stdout=PIPE)
			process.communicate()
			self.nft = nftnl_helper.nftnl_helper(1)

			self.addr = "2001:db08::1001"
			self.src_str = "2001:db08::1"
			self.dst_str = "2001:db08::2"
			self.src_addr = IPv6Address(self.src_str)
			self.dst_addr = IPv6Address(self.dst_str)
			self.dport = 80
			self.sport = 5001
			self.protocol = 6
	
			self.conn1 = Connection(source=self.src_addr, destination=self.dst_addr, sport=self.sport, dport=self.dport, protocol=self.protocol)
			
			self.default_rules_string = b"""table ip6 ipv6y {
	chain prerouting {
		type nat hook prerouting priority 1; policy accept;
	}

	chain postrouting {
		type nat hook postrouting priority 1; policy accept;
	}

	chain forward_nfqueue {
		type filter hook forward priority 1; policy accept;
		oif "lo" ct state new queue num 0
	}
}
"""

			self.nat_rules_string = b"""table ip6 ipv6y {
	chain prerouting {
		type nat hook prerouting priority 1; policy accept;
		iif "lo" ip6 saddr 2001:db08::1 tcp sport http tcp dport 5001 dnat to 2001:db08::1001
	}

	chain postrouting {
		type nat hook postrouting priority 1; policy accept;
		oif "lo" ip6 daddr 2001:db08::2 tcp sport 5001 tcp dport http snat to 2001:db08::1001
	}

	chain forward_nfqueue {
		type filter hook forward priority 1; policy accept;
		oif "lo" ct state new queue num 0
	}
}
"""
		def test_nft(self):
			self.assertEqual(self.default_rules_string, self.get_nft_rules(), "Wrong default rules!")
			snat_handle, dnat_handle = self.nft.add_nat_pair(b"ipv6y", self.conn1.protocol, self.conn1.source, self.conn1.destination, self.addr, self.conn1.sport, self.conn1.dport)
			self.assertEqual(self.nat_rules_string, self.get_nft_rules(), "Nat rules not matching!")
			self.nft.rule_del(b"ipv6y", b"postrouting", snat_handle)
			self.nft.rule_del(b"ipv6y", b"prerouting", dnat_handle)
			self.assertEqual(self.default_rules_string, self.get_nft_rules(), "Could not delete nat rules!")

		def get_nft_rules(self):
			process = Popen(["nft", "list", "table", "ip6", "ipv6y"], stdout=PIPE)
			output, err = process.communicate()
			return output

		def tearDown(self):
			process = Popen(["nft", "delete", "table", "ip6", "ipv6y"], stdout=PIPE)
			process.communicate()

if __name__ == "__main__":
	unittest.main()
