/*
 * This file is part of ipv6y 
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * No documentation since this will be removed in a future release!
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>
#include <libnetfilter_conntrack/libnetfilter_conntrack_tcp.h>

#include <pthread.h>

#ifndef IPPROTO_ICMPV6
#define IPPROTO_ICMPV6 0x3A
#endif

#ifdef DEBUG_OUTPUT
#define PRINTF_DEBUG printf
#else
#define PRINTF_DEBUG
#endif

struct nfct_handle *h;
pthread_t myThread;
struct nf_conntrack *last_ct;

struct connection_info_ipv6 {
	uint32_t src[4];
	uint32_t dst[4];
	uint16_t sport;
	uint16_t dport;
	uint8_t l4proto;
};

struct queue_node_s{
	struct queue_node_s* next;
	void* data;
};

struct queue_s{
	struct queue_node_s* first;
	struct queue_node_s* last;
};

typedef struct queue_s queue_t;

int queue_empty(queue_t* queue){
	return (queue == NULL || queue->first == NULL);
}

void* queue_dequeue(queue_t* queue){
	if(queue == NULL){
		return NULL;
	}
	struct queue_node_s* node = queue->first;

	queue->first = node->next;

	if(queue->first == NULL){ //no elements left
		queue->last = NULL;
	}

	void* data = node->data;

	free(node);

	return data;
}

int queue_enqueue(queue_t* queue, void* data){
	if(queue == NULL){
		return 1;
	}
	struct queue_node_s* node = malloc(sizeof(struct queue_node_s));
	node->data = data;
	node->next = NULL;

	if(queue->last == NULL){
		queue->first = queue->last = node; //no element in queue
	} else {
		queue->last->next = node;
		queue->last = node;
	}
	return 0;
}

int queue_delete(queue_t* queue){
	if(queue == NULL){
		return 1;
	}

	while(!queue_empty(queue)){
		queue_dequeue(queue);
	}

	free(queue);
	return 0;
}

queue_t* queue_create(){
	queue_t* queue = malloc(sizeof(queue_t));
	queue->first = queue->last = NULL;

	return queue;
}

queue_t* connectionList = NULL;

int event_cb(enum nf_conntrack_msg_type type,
			struct nf_conntrack *ct,
			void *data)
{
	char buf[1024];

	nfct_snprintf(buf, sizeof(buf), ct, type, NFCT_O_PLAIN, NFCT_OF_TIME);
	PRINTF_DEBUG("%s\n", buf);

	uint8_t proto = nfct_get_attr_u8(ct, ATTR_L3PROTO);

	//TODO: ipv4 enum
	if(proto == 4){
		return NFCT_CB_CONTINUE;
	}

	struct connection_info_ipv6* connection = malloc(sizeof(struct connection_info_ipv6));

	memcpy(&connection->src[0] ,nfct_get_attr(ct, ATTR_IPV6_SRC), sizeof(uint32_t)*4);
	memcpy(&connection->dst[0] ,nfct_get_attr(ct, ATTR_IPV6_DST), sizeof(uint32_t)*4);
	connection->sport = ntohs(nfct_get_attr_u16(ct, ATTR_PORT_SRC));
	connection->dport = ntohs(nfct_get_attr_u16(ct, ATTR_PORT_DST));
	connection->l4proto = nfct_get_attr_u8(ct, ATTR_L4PROTO);

	int i;
	for(i = 0; i < 4; ++i){
		connection->src[i] = ntohl(connection->src[i]);
		connection->dst[i] = ntohl(connection->dst[i]);
	}

	queue_enqueue(connectionList, connection);


	return NFCT_CB_CONTINUE;
}

void* helper(void* data){
	nfct_catch(h);
	return 0;
}

int isAvailable(){
	return !queue_empty(connectionList);
}

struct connection_info_ipv6 getConnectionInfo(){
	struct connection_info_ipv6* info = queue_dequeue(connectionList);

	struct connection_info_ipv6 retVal;
	retVal.l4proto = -1;

	if(info != NULL){
		retVal = *info;
		free(info);
	}

	return retVal;
}

int start(void){

	connectionList = queue_create();

	struct nfct_filter *filter;

	h = nfct_open(CONNTRACK, NF_NETLINK_CONNTRACK_DESTROY);
	if (!h) {
		perror("nfct_open");
		return 0;
	}

	filter = nfct_filter_create();
	if (!filter) {
		perror("nfct_create_filter");
		return 0;
	}

	nfct_filter_add_attr_u32(filter, NFCT_FILTER_L4PROTO, IPPROTO_UDP);
	nfct_filter_add_attr_u32(filter, NFCT_FILTER_L4PROTO, IPPROTO_TCP);
	nfct_filter_add_attr_u32(filter, NFCT_FILTER_L4PROTO, IPPROTO_ICMPV6);

	/* BSF always wants data in host-byte order */
	struct nfct_filter_ipv4 filter_ipv4 = {
		.addr = 0,
		.mask = 0,
	};


	/* ignore whatever that comes from ipv4 */
	nfct_filter_set_logic(filter,
			NFCT_FILTER_SRC_IPV4,
			NFCT_FILTER_LOGIC_NEGATIVE);

	nfct_filter_add_attr(filter, NFCT_FILTER_SRC_IPV4, &filter_ipv4);

	/* BSF always wants data in host-byte order */
	struct nfct_filter_ipv6 filter_ipv6 = {
		.addr = { 0xfe800000, 0x00000000, 0x00000000, 0x00000000 },
		.mask = { 0xffc00000, 0x00000000, 0x00000000, 0x00000000 },
	};

	/* ignore whatever that comes from fe80::/10 */
	nfct_filter_set_logic(filter,
			NFCT_FILTER_SRC_IPV6,
			NFCT_FILTER_LOGIC_NEGATIVE);

	nfct_filter_add_attr(filter, NFCT_FILTER_SRC_IPV6, &filter_ipv6);

	if (nfct_filter_attach(nfct_fd(h), filter) == -1) {
		perror("nfct_filter_attach");
		return 0;
	}

	/* release the filter object, this does not detach the filter */
	nfct_filter_destroy(filter);

	nfct_callback_register(h, NFCT_T_ALL, event_cb, NULL);

	pthread_create(&myThread, NULL, helper, NULL);

	return 0;
}

void stop(){
	nfct_close(h);
}
